package com.example;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = "nonImportantField")
class Message {
    private String text;
    private int timesSent;
    private String nonImportantField;
}
