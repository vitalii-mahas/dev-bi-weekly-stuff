package com.example.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
@Value
@EqualsAndHashCode(callSuper = false)
public class User extends AbstractDocument {
    private String username;

    @Indexed(unique = true)
    private String email;

    @Field("user-age")
    private int age;
}
