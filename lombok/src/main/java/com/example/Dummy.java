package com.example;

import lombok.Data;

@Data
public class Dummy {
    private String value;

    @Override
    public String toString() {
        return "Dummy{" +
                "value='" + value + '\'' +
                '}';
    }

    public static void main(String[] args) {
        System.out.println(new Dummy());
    }
}
