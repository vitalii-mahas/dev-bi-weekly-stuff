package com.example;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AddressTest {

    @Test
    public void shouldHaveNoArgsConstructor() {
        Address address = new Address();

        assertThat(address.getStreet()).isNull();
        assertThat(address.getPostCode()).isNull();
        assertThat(address.getHouseNumber()).isNull();
    }

    @Test
    public void shouldHaveConstructorForFinalProperties() {
        Address address = new Address("Long street", "2525 AB");

        assertThat(address.getStreet()).isEqualTo("Long street");
        assertThat(address.getPostCode()).isEqualTo("2525 AB");
        assertThat(address.getHouseNumber()).isNull();
    }

    @Test
    public void shouldHaveAllArgsConstructor() {
        Address address = new Address("Long street", "2525 AB", "123");

        assertThat(address.getStreet()).isEqualTo("Long street");
        assertThat(address.getPostCode()).isEqualTo("2525 AB");
        assertThat(address.getHouseNumber()).isEqualTo("123");
    }
}