# Lombok

This project shows the usage of Lombok java library with a handful tests to demonstrate the expected generated output.

See also `lombok.config` file for few setting options set for Lombok.
