package com.example;

import lombok.Data;

@Data
class CreateUserDto {
    private final String userName;
    private String firstName;
    private String lastName;
}
