package com.general;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

class DatesHamcrestTest {

    @Test
    void shouldCompareDateWithoutTime() {
        Date date = dateFromSomeService();

        Calendar actual = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        actual.setTime(date);

        assertThat(actual.get(Calendar.YEAR), equalTo(2015));
        assertThat(actual.get(Calendar.MONTH), equalTo(Calendar.APRIL));
        assertThat(actual.get(Calendar.DAY_OF_MONTH), equalTo(16));
    }

    @Test
    void shouldCompareDateIncludingTime() {
        Date date = dateFromSomeService();

        Calendar actual = Calendar.getInstance();
        actual.setTime(date);

        assertThat(actual.get(Calendar.YEAR), equalTo(2015));
        assertThat(actual.get(Calendar.MONTH), equalTo(Calendar.APRIL));
        assertThat(actual.get(Calendar.DAY_OF_MONTH), equalTo(16));
        assertThat(actual.get(Calendar.HOUR_OF_DAY), equalTo(23));
        assertThat(actual.get(Calendar.MINUTE), equalTo(55));
    }

    @Disabled
    @Test
    void failingDates() {
        Date date = dateFromSomeService();

        Calendar actual = Calendar.getInstance();
        actual.setTime(date);

        assertThat(actual.get(Calendar.YEAR), equalTo(2014));
        assertThat(actual.get(Calendar.MONTH), equalTo(Calendar.APRIL));
        assertThat(actual.get(Calendar.DAY_OF_MONTH), equalTo(16));
        assertThat(actual.get(Calendar.HOUR_OF_DAY), equalTo(23));
        assertThat(actual.get(Calendar.MINUTE), equalTo(55));
    }

    private Date dateFromSomeService() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, Calendar.APRIL);
        calendar.set(Calendar.DATE, 16);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 55);
        calendar.set(Calendar.SECOND, 42);
        calendar.set(Calendar.MILLISECOND, 123);
        return calendar.getTime();
    }
}
