package com.example;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
class UserDto {

    @Setter(value = AccessLevel.PRIVATE)
    private Long id;

    @Setter
    private String name;
}
