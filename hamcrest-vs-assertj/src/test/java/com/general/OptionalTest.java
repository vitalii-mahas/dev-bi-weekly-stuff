package com.general;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class OptionalTest {

    @Test
    void shouldShowHowCoolAssertjIs() {
        assertThat(Optional.of(123)).contains(123);
    }
}
