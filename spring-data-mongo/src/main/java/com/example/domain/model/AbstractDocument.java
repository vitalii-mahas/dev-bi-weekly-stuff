package com.example.domain.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

@Getter
abstract class AbstractDocument {

    @Id
    @Setter(AccessLevel.PRIVATE)
    private String id;

}
