package com.example;

import lombok.Value;

@Value
class ImmutableObject {
    private String label;
    private Integer size;
}
