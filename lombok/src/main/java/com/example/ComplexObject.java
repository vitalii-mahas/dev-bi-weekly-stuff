package com.example;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.util.List;

@Builder
@Getter
class ComplexObject {
    private String name;
    private String phone;
    private String label;
    @Singular
    private List<String> orders;
}
