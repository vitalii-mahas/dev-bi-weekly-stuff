package com.email;


import org.junit.jupiter.api.Test;

import java.util.Date;

import static com.email.SenderMother.anySender;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;

class EmailSenderServiceHamcrestTest {

    private EmailSenderService service = new EmailSenderService();

    @Test
    void shouldInitializeSendOnFieldWithCurrentDate() {
        EmailMessage message = message();

        service.send(message);

        assertThat(message.getSendOn(), lessThanOrEqualTo(new Date()));
    }

    private EmailMessage message() {
        return new EmailMessage(
                "message",
                anySender()
        );
    }

}