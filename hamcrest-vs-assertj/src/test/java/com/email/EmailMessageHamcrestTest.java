package com.email;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.IsEqual.equalTo;

 class EmailMessageHamcrestTest {
    private static final String MESSAGE_BODY = "some text message";
    private Recipient john = RecipientMother.recipient("john@host.com", "Mr. Smith");
    private Recipient jane = RecipientMother.recipient("jane@host.com", "Mrs. Fox");

    @Test
    void shouldCreateNewMessage() {
        Sender sender = SenderMother.anySender();
        EmailMessage emailMessage = new EmailMessage(
                MESSAGE_BODY,
                sender
        );

        emailMessage.addRecipient(john);

        assertThat(emailMessage.getMessage(), equalTo(MESSAGE_BODY));
        assertThat(emailMessage.getFrom(), equalTo(sender));
        assertThat(emailMessage.getRecipients(), containsInAnyOrder(john));
        assertThat(emailMessage.getSendOn(), nullValue());
    }

    @Test
    void shouldCreateNewMessageWithMultipleRecipients() {
        Sender sender = SenderMother.anySender();
        EmailMessage emailMessage = new EmailMessage(
                MESSAGE_BODY,
                sender
        );

        emailMessage.addRecipient(john);
        emailMessage.addRecipient(jane);

        assertThat(emailMessage.getRecipients(), containsInAnyOrder(john, jane));
    }

    @Nested
    @Disabled
    class ShowErrorMessages {

        @Test
        void shouldCreateNewMessageFailure() {
            Sender sender = SenderMother.anySender();
            EmailMessage emailMessage = new EmailMessage(
                    MESSAGE_BODY + "dummy",
                    sender
            );

            emailMessage.addRecipient(john);

            assertThat(emailMessage.getMessage(), equalTo(MESSAGE_BODY));
        }

        @Test
        void shouldCreateNewMessageWithMultipleRecipientsFailure() {
            Sender sender = SenderMother.anySender();
            EmailMessage emailMessage = new EmailMessage(
                    MESSAGE_BODY,
                    sender
            );

            emailMessage.addRecipient(john);
            emailMessage.addRecipient(jane);
            emailMessage.addRecipient(null);

            assertThat(emailMessage.getRecipients(), containsInAnyOrder(jane));
        }
    }


}