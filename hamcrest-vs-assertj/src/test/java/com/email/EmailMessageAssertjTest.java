package com.email;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class EmailMessageAssertjTest {
    private static final String MESSAGE_BODY = "some text message";
    private Recipient john = RecipientMother.recipient("john@host.com", "Mr. Smith");
    private Recipient jane = RecipientMother.recipient("jane@host.com", "Mrs. Fox");

    @Test
    void shouldCreateNewMessage() {
        Sender sender = SenderMother.anySender();
        EmailMessage emailMessage = new EmailMessage(
                MESSAGE_BODY,
                sender
        );

        emailMessage.addRecipient(john);

        assertThat(emailMessage.getMessage()).isEqualTo(MESSAGE_BODY);
        assertThat(emailMessage.getFrom()).isEqualTo(sender);
        assertThat(emailMessage.getRecipients()).containsExactlyInAnyOrder(john);
        assertThat(emailMessage.getSendOn()).isNull();
    }

    @Test
    void shouldCreateNewMessageWithMultipleRecipients() {
        Sender sender = SenderMother.anySender();
        EmailMessage emailMessage = new EmailMessage(
                MESSAGE_BODY,
                sender
        );

        emailMessage.addRecipient(john);
        emailMessage.addRecipient(jane);

        assertThat(emailMessage.getRecipients())
                .containsExactlyInAnyOrder(john, jane);
    }

    @Nested
    @Disabled
    class ShowErrorMessages {

        @Test
        void shouldCreateNewMessageFailure() {
            Sender sender = SenderMother.anySender();
            EmailMessage emailMessage = new EmailMessage(
                    MESSAGE_BODY + "dummy",
                    sender
            );

            emailMessage.addRecipient(john);

            assertThat(emailMessage.getMessage()).isEqualTo(MESSAGE_BODY);
        }

        @Test
        void shouldCreateNewMessageWithMultipleRecipientsFailure() {
            Sender sender = SenderMother.anySender();
            EmailMessage emailMessage = new EmailMessage(
                    MESSAGE_BODY,
                    sender
            );

            emailMessage.addRecipient(john);
            emailMessage.addRecipient(jane);
            emailMessage.addRecipient(null);

            assertThat(emailMessage.getRecipients())
                    .containsExactlyInAnyOrder(john);
        }
    }

}