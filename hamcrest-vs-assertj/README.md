# Hamcrest Vs AssertJ

This is a demo repository to show test examples using Hamcrest Matchers
and AssertJ.

## Structure of the project
This is a simple Maven project which contains a simplified example of email sending functionality
as well as few simple tests which just show similarities and differences between testing libraries.

Production code is presented by the classes:

* Sender - The person who sends a message.
* Recipient - The person who receives a message.
* EmailMessage - Holder for message body, sender, list of recipients and date of sending.
* EmailSenderService - Service which sends a message and puts date of sending on the message.

Test code for aforementioned classes can be found under `src/test/java/com/email` folder.

Basic comparison of Hamcrest and AssertJ can be found under `src/test/java/com/general`

