package com.example;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ComplexObjectTest {

    @Test
    public void shouldNotHaveDefaultConstructor() {
        assertThatThrownBy(() ->
                ComplexObject.class.getConstructor()
        ).isInstanceOf(NoSuchMethodException.class);
    }

    @Test
    public void shouldHaveConstructorWithAllParameters() {
        new ComplexObject("name", "phone", "label", Collections.emptyList());
    }

    @Test
    public void shouldHaveBuilder() {
        ComplexObject object = ComplexObject.builder()
                .label("label")
                .name("name")
                .phone("phone")
                .order("order #1")
                .order("order #2")
                .build();

        assertThat(object.getLabel()).isEqualTo("label");
        assertThat(object.getName()).isEqualTo("name");
        assertThat(object.getPhone()).isEqualTo("phone");
        assertThat(object.getOrders()).containsExactly("order #1", "order #2");
    }
}