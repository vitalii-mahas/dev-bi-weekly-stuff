package com.example.domain.model.repository;

import com.example.domain.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.stream.Stream;

/**
 * See https://docs.spring.io/spring-data/mongodb/docs/2.0.5.RELEASE/reference/html/#repositories.query-methods.details
 * for method name rules
 */
public interface UserRepository extends CrudRepository<User, String> {

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameAndAge(String username, int age);

    Optional<User> findByUsernameAndEmail(String username, String email);

    List<User> findByAge(int age);

    List<User> findAllByAge(int age, Pageable pageable);

    @Query("{ email: {$regex: 'gmail.com$', $options: 'i'}}")
    Stream<User> streamUsersWithGmail();

    @Async
    Future<User> findByEmail(String email);
}
