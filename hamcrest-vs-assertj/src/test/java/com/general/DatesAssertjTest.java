package com.general;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

class DatesAssertjTest {

    @Test
    void shouldCompareDateWithoutTime() {
        Date date = dateFromSomeService();

        assertThat(date).isEqualToIgnoringHours("2015-04-16");
    }

    @Test
    void shouldCompareDateIncludingTime() {
        Date date = dateFromSomeService();

        assertThat(date).isEqualTo("2015-04-16 23:55:42.123");
    }

    @Disabled
    @Test
    void failingTest() {
        Date date = dateFromSomeService();

        assertThat(date).isEqualTo("2014-05-15 00:01:02.003");
    }

    private Date dateFromSomeService() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, Calendar.APRIL);
        calendar.set(Calendar.DATE, 16);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 55);
        calendar.set(Calendar.SECOND, 42);
        calendar.set(Calendar.MILLISECOND, 123);
        return calendar.getTime();
    }
}
