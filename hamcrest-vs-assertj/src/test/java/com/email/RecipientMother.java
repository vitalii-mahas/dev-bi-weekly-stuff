package com.email;

public class RecipientMother {

    public static Recipient recipient(String email, String name) {
        return new Recipient(email, name);
    }
}
