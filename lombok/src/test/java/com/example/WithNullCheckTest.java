package com.example;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class WithNullCheckTest {

    @Test
    public void shouldThrowExceptionOnNullValue() {
        assertThatThrownBy(() ->
                new WithNullCheck(null)
        ).isInstanceOf(NullPointerException.class);
    }
}