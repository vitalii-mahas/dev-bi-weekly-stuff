package com.email;

public class SenderMother {

    public static final String SENDER_EMAIL = "sender@host.com";
    public static final String SENDER_NAME = "Mr. Sender";

    public static Sender anySender() {
        return new Sender(SENDER_EMAIL, SENDER_NAME);
    }
}
