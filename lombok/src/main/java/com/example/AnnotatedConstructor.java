package com.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(onConstructor = @__(@MarkerAnnotation))
@AllArgsConstructor(staticName = "of")
@Getter
class AnnotatedConstructor {
    private String value;
}
