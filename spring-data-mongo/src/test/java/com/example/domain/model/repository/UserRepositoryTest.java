package com.example.domain.model.repository;

import com.example.domain.model.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class UserRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Override
    protected String getCollectionName() {
        return "user";
    }

    @Test
    public void shouldDummy() {
        userRepository.save(new User("admin", "any", 30));

        Optional<User> admin = userRepository.findByUsernameAndEmail("admin", null);

        Assertions.assertThat(admin).isEmpty();
    }

//    @Test
//    public void shouldBeEmptyByDefault() {
//        Optional<User> admin = userRepository.findByUsername("admin");
//
//        Assertions.assertThat(admin).isEmpty();
//    }

//    @Test
//    public void shouldFindUserAfterAdding() {
//        userRepository.save(new User("admin", "admin@example.com", 30));
//
//        Optional<User> admin = userRepository.findByUsername("admin");
//
//        Assertions.assertThat(admin).isPresent();
//    }

    @Test
    public void shouldFindUserByMultipleFields() {
        userRepository.save(new User("admin", "admin@example.com", 30));

        Optional<User> admin = userRepository.findByUsernameAndAge("admin", 30);

        Assertions.assertThat(admin).isPresent();
    }

    @Test
    public void shouldFindUsersByAge() {
        userRepository.save(new User("user-1", "1@1.com", 25));
        userRepository.save(new User("user-2", "2@2.com", 25));
        userRepository.save(new User("user-3", "3@3.com", 30));

        List<User> usersByAge = userRepository.findByAge(25);

        Assertions.assertThat(usersByAge)
                .extracting(User::getUsername)
                .containsExactly("user-1", "user-2");
    }

    @Test
    public void shouldFindUsersByAgeAndPage() {
        userRepository.save(new User("user-1", "1@1.com", 25));
        userRepository.save(new User("user-2", "2@2.com", 25));
        userRepository.save(new User("user-3", "3@3.com", 25));

        List<User> secondPage = userRepository.findAllByAge(25, PageRequest.of(1, 2));// second page for pages of size two

        Assertions.assertThat(secondPage)
                .extracting(User::getUsername)
                .containsExactly("user-3");
    }

    @Test
    public void shouldFindGmailUsers() {
        userRepository.save(new User("user-1", "1@gmail.com", 25));
        userRepository.save(new User("user-2", "2@GMAIL.COM", 25));
        userRepository.save(new User("user-3", "3@yahoo.com", 25));

        List<User> gmailUsers = userRepository.streamUsersWithGmail().collect(Collectors.toList());

        Assertions.assertThat(gmailUsers)
                .extracting(User::getUsername)
                .containsExactly("user-1", "user-2");
    }

    @Test
    public void shouldFetchDataAsynchronously() throws ExecutionException, InterruptedException {
        userRepository.save(new User("admin", "admin@example.com", 30));

        Future<User> future = userRepository.findByEmail("admin@example.com");

        // block on future
        User user = future.get();

        Assertions.assertThat(user).isEqualTo(
                new User("admin", "admin@example.com", 30));
    }
}