package com.example;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class CreateUserDtoTest {

    @Test
    public void shouldHaveConstructorForNonFinalFields() {
        new CreateUserDto("name");
    }

    @Test
    public void shouldNotHaveSetterForFinalFields() {
        assertThatThrownBy(() ->
                CreateUserDto.class.getMethod("setName", String.class)
        ).isInstanceOf(NoSuchMethodException.class);
    }

    @Test
    public void shouldHaveGettersAndSetters() {
        CreateUserDto dto = new CreateUserDto("name");
        dto.setFirstName("John");
        dto.setLastName("Doe");

        assertThat(dto.getUserName()).isEqualTo("name");
        assertThat(dto.getFirstName()).isEqualTo("John");
        assertThat(dto.getLastName()).isEqualTo("Doe");
    }

    @Test
    public void shouldHaveEqualsAndHashCode() {
        Set<CreateUserDto> dtos = new HashSet<>();
        dtos.add(new CreateUserDto("name"));
        dtos.add(new CreateUserDto("name"));

        assertThat(dtos).hasSize(1);
    }

    @Test
    public void shouldHaveToString() {
        CreateUserDto dto = new CreateUserDto("name");

        assertThat(dto.toString())
                .startsWith(CreateUserDto.class.getSimpleName())
                .contains("userName", "firstName", "lastName");
    }
}