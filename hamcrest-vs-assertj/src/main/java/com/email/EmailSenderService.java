package com.email;

import java.util.Date;

public class EmailSenderService {

    public void send(EmailMessage message) {
        // handle sending here

        message.setSendOn(new Date());
    }
}
