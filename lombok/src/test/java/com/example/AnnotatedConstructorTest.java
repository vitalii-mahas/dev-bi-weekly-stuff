package com.example;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.lang.reflect.Constructor;

public class AnnotatedConstructorTest {

    @Test
    public void shouldHaveAnnotationOnConstructor() throws NoSuchMethodException {
        Constructor<AnnotatedConstructor> constructor = AnnotatedConstructor.class.getConstructor();

        MarkerAnnotation annotation = constructor.getAnnotation(MarkerAnnotation.class);

        Assertions.assertThat(annotation).isNotNull();
    }

    @Test
    public void shouldHaveStaticMethodForInstanceCreation() {
        AnnotatedConstructor hello = AnnotatedConstructor.of("hello");

        Assertions.assertThat(hello.getValue()).isEqualTo("hello");
    }
}