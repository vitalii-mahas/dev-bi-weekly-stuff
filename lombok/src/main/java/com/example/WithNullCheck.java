package com.example;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
class WithNullCheck {

    @NonNull
    private final String alwaysPresentString;
}
