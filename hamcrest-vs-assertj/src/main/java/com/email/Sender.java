package com.email;

import java.util.Objects;

public class Sender {
    private final String email;
    private final String name;

    public Sender(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sender sender = (Sender) o;
        return Objects.equals(email, sender.email) &&
                Objects.equals(name, sender.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, name);
    }

    @Override
    public String toString() {
        return "Sender{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
