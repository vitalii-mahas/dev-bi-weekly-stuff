package com.email;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmailMessage {
    private final String message;
    private final Sender from;
    private List<Recipient> recipients = new ArrayList<>();
    private Date sendOn;

    public EmailMessage(String message, Sender from) {
        this.message = message;
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public Sender getFrom() {
        return from;
    }

    public List<Recipient> getRecipients() {
        return recipients;
    }

    public void addRecipient(Recipient recipient) {
        this.recipients.add(recipient);
    }

    public void setSendOn(Date sendOn) {
        this.sendOn = sendOn;
    }

    public Date getSendOn() {
        return sendOn;
    }
}
