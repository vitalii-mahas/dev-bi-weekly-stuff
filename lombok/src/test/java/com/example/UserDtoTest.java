package com.example;

import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;

public class UserDtoTest {

    @Test
    public void shouldCreateGetters() {
        UserDto userDto = new UserDto();
        userDto.setName("John");

        assertThat(userDto.getName()).isEqualTo("John");
    }

    @Test
    public void shouldHavePublicGettersByDefault() throws NoSuchMethodException {
        Method getName = UserDto.class.getDeclaredMethod("getName");
        Method getId = UserDto.class.getDeclaredMethod("getId");

        assertThat(getName).isNotNull();
        assertThat(Modifier.isPublic(getName.getModifiers())).isTrue();
        assertThat(getId).isNotNull();
        assertThat(Modifier.isPublic(getId.getModifiers())).isTrue();
    }

    @Test
    public void shouldCreateIdSetterWithPrivateAccessLevel() throws NoSuchMethodException {
        Method setId = UserDto.class.getDeclaredMethod("setId", Long.class);

        assertThat(setId).isNotNull();
        assertThat(Modifier.isPrivate(setId.getModifiers())).isTrue();
    }
}