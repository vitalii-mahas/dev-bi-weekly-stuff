package com.example;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ImmutableObjectTest {

    @Test
    public void shouldNotHaveDefaultConstructor() {
        assertThatThrownBy(() ->
                ImmutableObject.class.getConstructor()
        ).isInstanceOf(NoSuchMethodException.class);
    }

    @Test
    public void shouldNotHaveSetters() {
        assertThat(ImmutableObject.class.getMethods())
                .extracting(Method::getName)
                .allMatch(name -> !name.startsWith("set"));
    }

    @Test
    public void shouldContainAllPropertiesInConstructor() {
        ImmutableObject object = new ImmutableObject("hello", 123);

        assertThat(object.getLabel()).isEqualTo("hello");
        assertThat(object.getSize()).isEqualTo(123);
    }

    @Test
    public void shouldHaveEqualsAndHashCode() {
        Set<ImmutableObject> objects = new HashSet<>();

        objects.add(new ImmutableObject("hello", 123));
        objects.add(new ImmutableObject("hello", 123));

        assertThat(objects).hasSize(1);
    }
}