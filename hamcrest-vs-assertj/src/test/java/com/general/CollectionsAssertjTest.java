package com.general;

import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

class CollectionsAssertjTest {

    @Test
    void showExtracting() {
        List<Person> people = asList(new Person("Jack"), new Person("John"));

        assertThat(people)
                .extracting(Person::getName)
                .containsOnly("Jack", "John");
    }

    static class Person {

        private final String name;

        Person(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
