package com.example.domain.model.repository;

import com.example.MongoConfig;
import com.mongodb.BasicDBObject;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = MongoConfig.class)
public abstract class AbstractRepositoryTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void cleanupCollection() {
        mongoTemplate.getCollection(getCollectionName())
                .deleteMany(new BasicDBObject());
    }

    protected abstract String getCollectionName();
}
