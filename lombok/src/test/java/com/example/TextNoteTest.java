package com.example;

import org.junit.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class TextNoteTest {

    @Test
    public void shouldCompareByEquals() {
        TextNote first = textNote(1L, "my note", LocalDate.now());
        TextNote second = textNote(1L, "my note", LocalDate.now());

        assertThat(first).isEqualTo(second);
    }

    @Test
    public void shouldCalculateHashCode() {
        TextNote first = textNote(1L, "first", LocalDate.now());
        TextNote second = textNote(2L, "second", LocalDate.now());

        assertThat(first.hashCode()).isNotEqualTo(second.hashCode());
    }

    @Test
    public void shouldHaveSameHashCodeForTheSameSetOfFields() {
        Set<TextNote> notes = new HashSet<>();

        notes.add(textNote(1L, "text", LocalDate.now()));
        notes.add(textNote(1L, "text", LocalDate.now()));

        assertThat(notes).hasSize(1);
    }

    private TextNote textNote(Long id, String content, LocalDate date) {
        TextNote note = new TextNote();
        note.setId(id);
        note.setContent(content);
        note.setCreatedOn(date);
        return note;
    }
}