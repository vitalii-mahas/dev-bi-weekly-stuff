package com.general;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class StringsAssertjTest {
    @Test
    void shouldContainSubstring() {
        assertThat("ABC").contains("AB");
    }

    @Test
    void shouldEndWith() {
        assertThat("ABC").endsWith("BC");
    }

    @Test
    void shouldStartWith() {
        assertThat("ABC").startsWith("AB");
    }
}
