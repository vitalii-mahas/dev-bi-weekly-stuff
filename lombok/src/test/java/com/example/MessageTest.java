package com.example;


import org.assertj.core.api.Assertions;
import org.junit.Test;

public class MessageTest {

    @Test
    public void shouldGenerateToString() {
        Message message = new Message();
        message.setText("long long description");
        message.setTimesSent(5);
        message.setNonImportantField("blah");

        System.out.println(message);

        Assertions.assertThat(message.toString())
                .startsWith(Message.class.getSimpleName())
                .contains("text")
                .contains("timesSent")
                .doesNotContain("nonImportantField");
    }
}