package com.general;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;

class StringsHamcrestTest {

    @Test
    void shouldContainSubstring() {
        assertThat("ABC", containsString("AB"));
    }

    @Test
    void shouldEndWith() {
        assertThat("ABC", endsWith("BC"));
    }

    @Test
    void shouldStartWith() {
        assertThat("ABC", startsWith("AB"));
    }
}
