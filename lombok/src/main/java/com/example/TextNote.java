package com.example;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
class TextNote extends AbstractNote {
    private String content;
    private LocalDate createdOn;
}
